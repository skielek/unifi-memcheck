# unifi-memcheck

This script is intended to monitor memory conditions via cron on the Unifi controller and to take preventive measures before out-of-memory conditions lead to an unplanned service outage.

It's important to understand that this is not a "fix" for a specific problem. There are one or more conditions present in recent Unifi applications that have resulted in out-of-memory situations that result in a full controller crash. With this script we try to detect these conditions early enough to gracefully restart the affected application before a crash occurs. This typically results in a short interruption to the offending application that in most cases users cannot perceive, whilst ideally preventing a wider impacting outage across all applications and all users.

## Installation

By default this is being installed in the /root directory and will execute from cron once per minute.  The script will log any events/actions taken into **/root/mem.log**. To get started, SSH into the Unifi controller as root. If you've not enabled SSH access, you'll need to do that first from the Web UI.

Once logged in as root, you can copy+paste the following commands to install the script and activate the cron job:

`unifi-os shell`

`cd /root`

`wget https://gitlab.com/skielek/unifi-memcheck/-/raw/master/unifi-memcheck.sh`

`chmod +x unifi-memcheck.sh`

`crontab -l | { cat; echo "* * * * * /root/unifi-memcheck.sh >/dev/null 2>&1"; } | crontab -`

## Post-Installation

You can check the **/root/mem.log** file to see if an event was triggered. The log will include the time it occured, the processes that were running at the time (useful to provide to Unifi support should you need to open a ticket), and the action the script had taken.
