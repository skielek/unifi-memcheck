#!/bin/sh
################################################################################
#      Author: Samuel Kielek <samuel@kielek.net>                               #
# Description: Detect UDM-Pro excessive memory utilisation and restart the     #
#              offending processes gracefully.                                 #
################################################################################

totalmem=$( free -m | awk '/Mem:/{print $2}' )       # total RAM
totalswp=$( free -m | awk '/Swap:/{print $2}' )      # total Swap

memused=$( free -m | awk '/Mem:/{print $3}' )        # RAM in use
swpused=$( free -m | awk '/Swap:/{print $3}' )       # Swap in use

# Default threshold to trigger a unifi core restart is memory utilisation
# greater than 90% AND swap utilisation greater than 30%. Under normal
# conditions my system never exceeds those values. But if you want different
# values you may modify these variables to suit your needs.

memthresh=$(( 9 * ( $totalmem / 10 ) ))              # RAM trigger theshold
swpthresh=$(( 3 * ( $totalswp / 10 ) ))              # Swap trigger threshold

log='/root/mem.log'                                  # location to log events

touch $log

action=0                                             # initialise action

if [ "$memused" -gt $memthresh ] && [ "$swpused" -gt $swpthresh ]; then
	echo "$(date +'%Y-%m-%d %H:%M')		Detected high memory utilisation ($memused MB memory / $swpused MB swap used)" >> $log
	ps auxww --forest --sort rss >> $log
	echo >> $log

	# Detect the usual suspects and then restart them if appropriate
	echo -n "unifi-core = " >> $log   ; ps auxww --sort rss | tail -1 | egrep -q "unifi-core.*service\.js";    if [ $? -eq 0 ]; then action=1; echo "RESTARTING" >> $log; systemctl restart unifi-core    --no-pager; else echo "OK" >> $log; fi
	echo -n "unifi-protect = " >> $log; ps auxww --sort rss | tail -1 | egrep -q "unifi-protect.*service\.js"; if [ $? -eq 0 ]; then action=1; echo "RESTARTING" >> $log; systemctl restart unifi-protect --no-pager; else echo "OK" >> $log; fi

	echo "\n\n" >> $log

	# If we restarted a service and memory utilisation has dropped, then
        # attempt to flush pages from swap back into physical memory
	if [ $action -eq 1 ]  && [ "$memused" -lt $memthresh ] && [ "$swpused" -gt $swpthresh ]; then
		for i in $(seq 1 5); do
			swapoff -a
			sleep 5
		done
		swapon -a
	fi
fi

exit
